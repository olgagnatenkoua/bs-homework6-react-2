import React from "react";
import "./Header.css";

import dateTimeOptions from "../../helpers/dateTimeOptions";

class Header extends React.Component {
  render = () => {
    if (!this.props.header) {
      return null;
    }
    const { participants, messages, lastMessage } = this.props.header;

    const displayDate = lastMessage
      ? new Date(lastMessage).toLocaleString("en-us", dateTimeOptions)
      : "";
    return (
      <div className="chat__header">
        <div className="chat__label">
          <span>MY CHAT</span>
        </div>
        <div className="chat__participant-count">
          <span>{participants} Participants</span>
        </div>
        <div className="chat__message-count">
          <span>{messages} Messages</span>
        </div>
        <div className="chat__last-msg">
          <span>Last message at: {displayDate}</span>
        </div>
      </div>
    );
  };
}

export default Header;
