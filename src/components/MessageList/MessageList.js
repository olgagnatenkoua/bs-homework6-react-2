import React from "react";
import "./MessageList.css";
import Message from "../Message/Message";
import Divider from "../Divider/Divider";

class MessageList extends React.Component {
  makeChatComponents = (messages, handlers) => {
    const chatComponents = [];
    if (!(messages && messages.length)) return;
    messages.forEach((msg, idx) => {
      if (idx) {
        const curDate = new Date(msg.created_at + " GMT+0000");
        const prevDate = new Date(messages[idx - 1].created_at + " GMT+0000");
        let dividerText = "";
        if (
          curDate.toLocaleDateString("en-us") !==
          prevDate.toLocaleDateString("en-us")
        ) {
          const yestDate = new Date(
            new Date().setDate(new Date().getDate() - 1)
          );
          if (
            prevDate.toLocaleDateString("en-us") ===
            yestDate.toLocaleDateString("en-us")
          ) {
            dividerText = "Yesterday";
          } else {
            dividerText = prevDate.toLocaleDateString("en-us");
          }
          chatComponents.push(<Divider key={dividerText} text={dividerText} />);
        }
      }
      chatComponents.push(
        <Message key={msg.id} message={msg} handlers={handlers} />
      );
    });
    return chatComponents;
  };

  render() {
    const { messages, handlers } = this.props;
    return (
      <div className="chat__body">
        {messages ? this.makeChatComponents(messages, handlers) : null}
      </div>
    );
  }
}

export default MessageList;
