import React from "react";
import "./MakeMessage.css";
import constants from "../../helpers/constants";

class MakeMessage extends React.Component {
  constructor(props) {
    super(props);

    const { editedMessage } = this.props;
    const initialText = editedMessage ? editedMessage.message : "";
    this.state = {
      text: initialText
    };
  }

  onChange = evt => {
    this.setState({
      text: evt.target.value
    });
  };

  handleSendClick = () => {
    const { editedMessage } = this.props;
    const newMessageText = this.state.text;
    let newMessage = {};
    if (editedMessage) {
      newMessage = Object.assign({}, editedMessage);
      newMessage.message = newMessageText;
    } else {
      newMessage = {
        id: Date.now().toString(),
        user: constants.USER_NAME,
        avatar: constants.USER_AVATAR,
        created_at: new Date().toGMTString("en-us"), // store message date in UTC tiemzone
        message: newMessageText,
        likes: 0,
        ownedByMe: true,
        likedByMe: false
      };
    }
    this.props.makeMessage(newMessage);
    this.setState({
      text: ""
    });
  };

  render = () => {
    const { btnName } = this.props;
    console.log("MakeMessage", btnName);

    return (
      <div className="chat__edit">
        <div className="chat__text">
          <textarea
            name="send-message"
            id="send-message"
            cols="5"
            rows="3"
            className="chat__input"
            value={this.state.text}
            onChange={this.onChange}
          />
          <button
            type="button"
            className="chat__send"
            onClick={this.handleSendClick}
            disabled={!this.state.text}
          >
            <i className="far fa-paper-plane" />
            {btnName || "Send"}
          </button>
        </div>
      </div>
    );
  };
}

export default MakeMessage;
