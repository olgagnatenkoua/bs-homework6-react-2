import React from "react";
import "./Modal.css";

class Modal extends React.Component {
  renderCloseBtn = () => {
    const { onClose } = this.props;
    if (!onClose) return null;

    return (
      <button
        type="button"
        className="modal__btn modal__close"
        onClick={onClose}
      >
        <i className="fas fa-times" />
      </button>
    );
  };

  render = () => {
    const { open, title } = this.props;
    if (!open) {
      return null;
    }
    return (
      <div className="overlay">
        <div className="modal">
          <h2>{title || ""}</h2>
          <div className="modal__content">{this.props.children}</div>
          <div className="modal__buttons">{this.renderCloseBtn()}</div>
        </div>
      </div>
    );
  };
}

export default Modal;
