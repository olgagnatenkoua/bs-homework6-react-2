import {
  LIKE_MESSAGE,
  START_EDIT_MESSAGE,
  DELETE_MESSAGE,
  ADD_MESSAGE,
  COMPLETE_EDIT_MESSAGE,
  CANCEL_EDIT_MESSAGE,
  COMPLETE_FETCH_MESSAGES
} from "./actionTypes";

import initialState from "../../helpers/initialState";

export default function(state = initialState, action) {
  switch (action.type) {
    case COMPLETE_FETCH_MESSAGES: {
      const { messages } = action.payload;
      const enrichedMessages = messages.map(msg => {
        const updatedMsg = Object.assign(msg);
        delete updatedMsg.marked_read; // "marked_read" property is not needed for now
        updatedMsg.likedByMe = false;
        updatedMsg.ownedByMe = false;
        updatedMsg.likes = 0;
        return updatedMsg;
      });

      const participants = new Set(messages.map(msg => msg.user));
      let lastMessage = "Unknown";
      const lastMessageIdx = messages.length - 1;
      if (messages && messages[lastMessageIdx]) {
        lastMessage = new Date(
          messages[lastMessageIdx].created_at + " GMT+0000"
        );
      }

      return {
        ...state,
        messages: enrichedMessages,
        header: {
          participants: participants.size + 1,
          messages: messages.length,
          lastMessage
        }
      };
    }

    case LIKE_MESSAGE: {
      const { id } = action.payload;
      const { messages } = state;

      const updatedMessages = [...messages];
      const likedMessageIdx = updatedMessages.findIndex(msg => msg.id === id);
      if (likedMessageIdx === -1) {
        return state;
      }
      const likedMessage = updatedMessages[likedMessageIdx];
      likedMessage.likedByMe = !likedMessage.likedByMe;
      likedMessage.likes =
        likedMessage.likes + (likedMessage.likedByMe ? 1 : -1);
      return {
        ...state,
        messages: updatedMessages
      };
    }

    case DELETE_MESSAGE: {
      const { messages } = state;
      const { id } = action.payload;
      const deletedMessageIdx = messages.findIndex(msg => msg.id === id);
      if (deletedMessageIdx === -1) {
        return state;
      }
      const updatedMessages = [...messages];
      updatedMessages.splice(deletedMessageIdx, 1);

      return {
        ...state,
        messages: updatedMessages,
        header: {
          ...state.header,
          messages: state.header.messages - 1
        }
      };
    }

    case START_EDIT_MESSAGE: {
      const { message } = action.payload;
      return {
        ...state,
        editedMessage: message
      };
    }

    case COMPLETE_EDIT_MESSAGE: {
      const { messages } = state;
      const { message } = action.payload;
      const editedMsgIdx = messages.findIndex(msg => msg.id === message.id);
      const updatedMessages = [...messages];
      updatedMessages[editedMsgIdx] = message;
      return {
        ...state,
        messages: updatedMessages,
        editedMessage: null
      };
    }

    case CANCEL_EDIT_MESSAGE: {
      return {
        ...state,
        editedMessage: null
      };
    }

    case ADD_MESSAGE: {
      const { messages, header } = state;
      const updatedMessages = [...messages];
      updatedMessages.push(action.payload.message);
      return {
        ...state,
        messages: updatedMessages,
        header: {
          ...header,
          messages: header.messages + 1
        }
      };
    }

    default: {
      return state;
    }
  }
}
