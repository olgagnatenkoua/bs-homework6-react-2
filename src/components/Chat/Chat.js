import React from "react";
import { connect } from "react-redux";
import * as actions from "./actions";

import { messageService } from "../../services/messageService";
import Spinner from "../Spinner/Spinner";
import Header from "../Header/Header";
import MessageList from "../MessageList/MessageList";
import MakeMessage from "../MakeMessage/MakeMessage";

import Modal from "../Modal/Modal";

class Chat extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false
    };
  }

  componentDidMount() {
    this.setState({
      ...this.state,
      loading: true
    });

    try {
      messageService.getInitialMessages().then(messages => {
        this.props.fetchMessages(messages);
        this.setState({
          ...this.state,
          loading: false
        });
      });
    } catch (error) {
      console.log(error);
    }
  }

  componentDidUpdate(prevProps) {
    // only change scroll if message count changes
    const { header } = this.props;
    const prevHeader = prevProps.header;
    if (header.messages !== prevHeader.messages) {
      this.scrollToBottom();
    }
  }

  likeMessage = id => {
    this.props.likeMessage(id);
  };

  addMessage = id => {
    this.props.addMessage(id);
  };

  deleteMessage = id => {
    this.props.deleteMessage(id);
  };

  editMessage = id => {
    const editedMessage = this.props.messages.find(msg => msg.id === id);
    if (!editedMessage) return;
    this.props.startEditMessage(editedMessage);
  };

  completeEditMessage = message => {
    this.props.completeEditMessage(message);
  };

  cancelEditMessage = () => {
    this.props.cancelEditMessage();
  };

  scrollToBottom = () => {
    const chatElement = document.querySelector(".chat__body");
    if (chatElement) {
      chatElement.scrollTo(0, chatElement.scrollHeight);
    }
  };

  renderChat = ({ messages, header, launchedAt }) => {
    const handlers = {
      likeMessage: this.likeMessage,
      makeMessage: this.addMessage,
      deleteMessage: this.deleteMessage,
      editMessage: this.editMessage
    };
    const open = !!this.props.editedMessage;
    return (
      <div className="chat">
        <Header header={header} />
        <MessageList
          messages={messages}
          launchedAt={launchedAt}
          handlers={handlers}
        />
        <MakeMessage makeMessage={this.addMessage} />
        <Modal
          open={open}
          title="Edit Message"
          onClose={this.cancelEditMessage}
        >
          <MakeMessage
            makeMessage={this.completeEditMessage}
            editedMessage={this.props.editedMessage}
            btnName="OK"
          />
        </Modal>
      </div>
    );
  };

  render() {
    const { loading } = this.state;
    return (
      <div className="chat__container">
        {loading ? <Spinner /> : this.renderChat(this.props)}
      </div>
    );
  }
}

const mapStateToProps = state => {
  console.log("mapStateToProps", state);
  return state;
};

const mapDispatchToProps = {
  ...actions
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Chat);
