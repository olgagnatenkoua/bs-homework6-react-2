import constants from "../helpers/constants";

class MessageService {
  async getInitialMessages() {
    try {
      const endpoint = constants.MESSAGE_API_URI;
      const apiResult = await fetch(endpoint, {
        method: "GET"
      });
      const json = await apiResult.json();
      return json;
    } catch (error) {
      throw error;
    }
  }
}

export const messageService = new MessageService();
